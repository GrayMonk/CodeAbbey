def isprime(x):
	for i in range(3, int(x ** 0.5) + 1, 2):
		if x % i == 0:
			return False
	return True 

def result(values, x = 3, d = []):
	d.append(2)

	while len(d) <= 200000:
		if isprime(x):
			d.append(x)
		
		x += 2

	for x in values:
		print(d[x - 1], end = ' ')


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = list(map(lambda x: int(x), line))
			result(line)