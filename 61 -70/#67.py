file = open('input.txt', 'r')

i, spam = 0, ''


def funk(a):
	step = 0
	b, c, d = 0, 1, 0
	while b != a:
		step += 1
		d = b + c
		b = c
		c = d
	
	return str(step)

for line in file:
	space = line.split(' ')
	if i != 0:
		spam = spam + funk(int(space[0])) + ' '
	i += 1

print(spam)