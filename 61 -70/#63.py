def isprime(x):
	if x > 3 and x % 2 == 0:
		return False
	for i in range(3, int(x ** 0.5) + 1, 2):
		if x % i == 0:
			return False
	return True 

def result(x, prime = 2):
	if isprime(x):
		print(x, end = ' ')
	else:
		while x > 1:
			if x % prime == 0:
				print(prime, end = '')
				x /= prime
				if x != 1:
					print('*', end = '')
			else:
				while True:
					prime += 1
					if isprime(prime):
						break

	print('', end = ' ')

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(str(line)) > 6:
			result(int(line[0]))