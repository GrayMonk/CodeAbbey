def result(m):
	d = []

	d.append(0)
	d.append(1)
	
	while True:
		if d[-1] % m == 0:
			print(d.index(d[-1]), end = ' ')
			break
		else:
			d.append(d[-2] + d[-1])	


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = list(map(lambda x: int(x), line))
			for x in line:
				result(x)