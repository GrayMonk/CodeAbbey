def isprime(x):
	if x > 3 and x % 2 == 0:
		return False
	for i in range(3, int(x ** 0.5) + 1, 2):
		if x % i == 0:
			return False
	return True 

def result(values):
	count = 1

	if values[0] % 2 != 0:
		settings = (values[0], values[1], 2)
	else:
		settings = (values[0] + 1, values[1], 2)
	
	for x in range(settings[0], settings[1], settings[2]):
		if isprime(x):
			count += 1
	print(count, end = ' ')


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = list(map(lambda x: int(x), line))
			result(line)