def check(xt, strg = ''):
	xt.sort()
	strg = ''.join(xt)
	
	for d in vin:
		if d[0] in strg and d[1] in strg and d[2] in strg:
			return True
	return False

def result(moves):
	x, o = [], []
	flag, step = 0, 0

	for s in moves:
		step += 1
		if flag == 0:
			x.append(s)
			flag = 1
		else:
			o.append(s)
			flag = 0

		if len(x) > 2 and check(x) or len(o) > 2 and check(o):
				print(step, end = ' ')
				flag = 5
				break

	if flag != 5:
		print(0, end = ' ')


vin = ['123', '456', '789', '147', '258', '369', '159', '357']

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			result(line)