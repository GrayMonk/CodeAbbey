file = open('input.txt', 'r')

i, spam = 0, ''

def func(a):
	lhv = ['RS', 'SP', 'PR']
	rhv = ['SR', 'PS', 'RP']
	score1, score2 = 0, 0
	for x in a:
		if x in lhv:
			score1 += 1
		elif x in rhv:
			score2 += 1
	
	if score1 > score2:
		return str(1)
	else:
		return str(2)

for line in file:
	space = (line.strip()).split(' ')
	if i != 0:
		spam = spam + func(space) + ' '
	i += 1
print(spam)