file = open('input.txt', 'r')

i, k, spam = 0, 0, ''
d = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', \
	'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

def func(a, k):
	i = 0
	strg = ''
	for x in a:
		while True:
			if x == d[i]:
				strg = strg + str(d[i-k])
				i = -1
				break
			elif x == '.':
				strg = strg + str('.')
				i = -1
				break
			i += 1

	return strg

for line in file:
	space = (line.strip()).split(' ')
	if i == 0:
		k = int(space[1])
	else:
		for x in space:
			spam = spam + str(func(str(x), int(k))) + ' '
	i += 1
print(spam)