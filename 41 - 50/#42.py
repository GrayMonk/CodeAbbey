def result(hand, numeric = ['2', '3', '4', '5', '6', '7', '8', '9']):
	score, flag = 0, 0
	for card in hand:
		if card in numeric:
			score += int(card)
		elif card == 'A':
			flag += 1
		else:
			score += 10

	while flag != 0:
		if score + 11 > 21:
			score += 1
		else:
			score += 11
		flag -= 1

	if score > 21:
		print('Bust', end = ' ')
	else:
		print(str(score), end = ' ')


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			result(line)