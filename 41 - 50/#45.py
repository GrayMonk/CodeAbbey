def create_deck(deck = []):
	ranks = ['A', '2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K']
	suits = ['C', 'D', 'H', 'S']

	for s in suits:
		for r in ranks:
			deck.append(s+r)
	return deck


def result(values, i = 0):
	deck = create_deck()

	for x in values:
		if x > 52: x = x % 52
		temp = deck[x]
		deck[x] = deck[i]
		deck[i] = temp
		i += 1
	
	for x in deck:
		print(x, end = ' ')


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		line = list(map(lambda x: int(x), line))
		result(line)