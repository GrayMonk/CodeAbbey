file = open('input.txt', 'r')

i, spam = 0, ''

def funk(x):
    step = 0
    while x != 1:
        step += 1
        if x % 2 == 0:
            x /= 2
        else:
            x = x * 3 + 1
		
    return step

for line in file:
	space = line.split(' ')
	if i != 0:
		for x in space:
			spam = spam + str(funk(int(x))) + ' '
	i += 1

print(spam)