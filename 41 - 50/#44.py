file = open('input.txt', 'r')

i, spam = 0, ''

def funk(a, b):
	return str(round(a % 6) + round(b % 6) + 2)

for line in file:
	space = line.split(' ')
	if i != 0:
    		spam = spam + funk(int(space[0]), int(space[1])) + ' '
	i += 1
print(spam)