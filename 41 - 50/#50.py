file = open('input.txt', 'r')

i, spam = 0, ''

def funk(a):
    newstr = ''
    for x in a:
        if x.isalpha():
            newstr += x
    
    if newstr == newstr[::-1]:
        return 'Y' 
    else:
        return 'N'

for line in file:
    space = line.split(' ')
    strg = ''
    if i != 0:
        for x in space:
            strg = strg + x
        
        spam = spam + funk(strg.lower()) + ' '
    i += 1
print(spam)