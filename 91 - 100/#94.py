def func(a):
	return str(sum(list(map(lambda x: x*x, a))))

with open('input.txt', 'r') as file:
	spam = ''
	for line in file:
		space = (line.strip()).split(' ')
		if len(space) != 1:
			spam += func(list(map(lambda x: int(x), space))) + ' '
print(spam)