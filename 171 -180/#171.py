from math import tan, radians

def result(values, d = []):
    values = [int(values[0]), float(values[1])]
    alpha = 90 - (180 - values[1])
    d.append(str(round(tan(radians(alpha)) * values[0])))
    if len(d) == count: print(' '.join(d))


with open('input.txt', 'r') as file:
    for line in file:
        line = (line.strip()).split(' ')
        if len(line) == 1:
            count = int(line[0])
        else:
            result(line)