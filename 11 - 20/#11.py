import math
file = open('input.txt', 'r')

a = []
b = []
c = []
i = 0
j = 0
res = 0
spam = ''

for line in file:
	space = line.split(' ')
	if i == 0:
		space.remove(space[0])
	else:
		a.insert(i, space[0].strip())
		b.insert(i, space[1].strip())
		c.insert(i, space[2].strip())
	i += 1

i = 0

while i < len(a):
	res = int(a[i]) * int(b[i]) + int(c[i])
	while res >= 1:
		j = j + (res % 10)
		res = math.trunc(res/10)
	spam = spam + str(j) + ' '
	j = 0
	i += 1

print(spam)