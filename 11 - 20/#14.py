file = open('input.txt', 'r')


i, k, n, res = 0, 0, 0, 0
a, b = [], []

for line in file:
	space = line.split(' ')
	if i == 0:
		n = int(space[0].strip())
	else:
		a.insert(i, space[0].strip())
		b.insert(i, space[1].strip())
	i += 1

i = 0

res = res + n
while i < len(a):

	if a[i] == '+':
		res = res + int(b[i])
	elif a[i] == '-':
		res = res - int(b[i])
	elif a[i] == '*':
		res = res * int(b[i])
	elif a[i] == '/':
		res = res / int(b[i])
	elif a[i] == '%':
		res = res % int(b[i])

	i += 1
print(res)