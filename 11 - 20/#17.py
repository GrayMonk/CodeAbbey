file = open('input.txt', 'r')

a, b, i, c, d = 113, 10000007, 0, 0, 0

for line in file:
	space = line.split(' ')
	if d != 0:
		while i < len(space):
			c += int(space[i])
			c *= a
			c %= b
			i += 1
	d += 1

print(c)