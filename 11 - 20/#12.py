file = open('input.txt', 'r')

d1, d2, h1, h2, m1, m2, s1, s2 = [], [], [], [], [], [], [], []
i, spam = 0, ''

def date_diff(d1, d2, h1, h2, m1, m2, s1, s2):
	resl = ''
	day  = d2 - d1
	hour = h2 - h1
	mint = m2 - m1
	sec  = s2 - s1

	if hour < 0:
		day -= 1
		hour += 24

	if mint < 0:
		hour -= 1
		mint += 60 

	if sec  < 0:
		mint -= 1
		sec += 60

	resl = '(' + str(day) + ' ' + str(hour) + ' ' + str(mint) + ' ' + \
		str(sec) + ') '
	return resl

for line in file:
	space = line.split(' ')
	if i == 0:
		space.remove(space[0])
	else:
		d1.insert(i, space[0].strip())
		h1.insert(i, space[1].strip())
		m1.insert(i, space[2].strip())
		s1.insert(i, space[3].strip())
		d2.insert(i, space[4].strip())
		h2.insert(i, space[5].strip())
		m2.insert(i, space[6].strip())
		s2.insert(i, space[7].strip())
	i += 1

i = 0

while i < len(d1):
	spam = spam + date_diff(int(d1[i]), int(d2[i]), int(h1[i]), int(h2[i]), \
		int(m1[i]), int(m2[i]), int(s1[i]), int(s2[i]))
	i += 1

print(spam)