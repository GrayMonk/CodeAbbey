import math
file = open('input.txt', 'r')

def summ(number):
	j, k = 0, 0
	num = []

	while number >= 1:
		num.insert(0, math.trunc(number%10))
		number /= 10
	
	while j < len(num):
		k = k + num[j] * (j + 1)
		j += 1

	return k

i, k, n = 0, 0, 0
spam = ''
a = []

for line in file:
	space = line.split(' ')
	if i == 0:
		k = int(space[0].strip())
	else:
		while n < k:
			a.insert(n, space[n].strip())
			n += 1
	i += 1

i = 0

while i < len(a):
	spam = spam + str(summ(int(a[i]))) + ' '
	i += 1

print(spam)