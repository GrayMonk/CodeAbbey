file = open('input.txt', 'r')

i, spam = 0, ''

def funk(a):
    newstr = ''
    d = []
    opn_brackets = ['[', '{', '(', '<']
    cld_brackets = [']', '}', ')', '>']
    for x in a:
        if  x in opn_brackets or x in cld_brackets:
            newstr += x

    for x in newstr:
        if x in opn_brackets:
            d.append(x)
        else:
            if len(d) != 0:
                if   x == cld_brackets[0] and d[-1] == '[':
                    d.pop()
                elif x == cld_brackets[1] and d[-1] == '{':
                    d.pop()
                elif x == cld_brackets[2] and d[-1] == '(':
                    d.pop()
                elif x == cld_brackets[3] and d[-1] == '<':
                    d.pop()
                else:
                    return 0
            else:
                return 0

    if len(d) == 0:
        return 1
    else:
        return 0

for line in file:
    space = (line.strip()).replace(' ', '')
    if i != 0:
        spam = spam + str(funk(space)) + ' '
    i += 1
print(spam)