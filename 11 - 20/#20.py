file = open('input.txt', 'r')

i, a, k = 0, 0, 0
s, spam = '', ''

for line in file:
	space = line.split(' ')
	if i != 0:

		while a < len(space):
			spam = spam + space[a].strip()
			a += 1
		
		a = 0
		while a < len(spam):
			if spam[a] == 'a' or spam[a] == 'o' or spam[a] == 'u' \
				or spam[a] == 'i' or spam[a] == 'e' or spam[a] == 'y':
					k = k + 1
			a += 1
		
		s = s + str(k) + ' '
	k = 0
	spam = ''
	a = 0
	i += 1	

print(s)