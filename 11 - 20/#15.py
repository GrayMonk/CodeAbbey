file = open('input.txt', 'r')

i, min_num, max_num = 0, 0, 0
a = []

for line in file:
	space = line.split(' ')
	while i < len(space):
		a.insert(i, space[i].strip())
		i += 1

i = 0

while i < len(a):
	if min_num > int(a[i]):
		min_num = int(a[i])

	if max_num < int(a[i]):
		max_num = int(a[i])
	i += 1

print(str(max_num) + ' ' + str(min_num))