def fnd_num(data):
    x = data.index('?')
    for i in range(0, 10):
        data[x] = i
        if count(data) % 10 == 0:
            break
    print(''.join([str(x) for x in data]), end = ' ')

def swp_num(data):
    for i in range(1, len(data)):
        data = swap(data, i)
        if count(data) % 10 != 0:
            data = swap(data, i)
        else:
            break
    print(''.join([str(x) for x in data]), end = ' ')

def count(data, second = False, summ = 0):
    for x in data[::-1]:
        if not second:
            summ += x
            second = True
        else:
            x = (x * 2) if (x * 2) < 10 else (x * 2 - 9)
            summ += x
            second = False 
    return summ

def swap(data, i, s = 0):
    s = data[i-1]
    data[i-1] = data[i]
    data[i] = s
    return data

for line in open('input.txt'):
    if len(line) > 3:
        line = [int(x) if x != '?' else x for x in line.strip()]
        if '?' in line:
            fnd_num(line)
        else:
            swp_num(line)