def result(comb):
	if comb == [1, 2, 3, 4, 5]:
		print('small-straight', end = ' ')
	elif comb == [2, 3, 4, 5, 6]:
		print('big-straight', end = ' ')
	else:
		comb2 = list(set(comb))
		pair, three, four, yacht = False, False, False, False
		for x in comb2:
			if comb.count(x) == 2 and pair: pair += 1
			if comb.count(x) == 2 and not pair: pair  = True
			if comb.count(x) == 3: three = True
			if comb.count(x) == 4: four  = True
			if comb.count(x) == 5: yacht = True

		if pair and pair != 2 and not three:
			print('pair', end = ' ')
		elif pair == 2:
			print('two-pairs', end = ' ')
		elif pair and three:
			print('full-house', end = ' ')
		elif not pair and three:
			print('three', end = ' ')
		elif four:
			print('four', end = ' ')
		elif yacht:
			print('yacht', end = ' ')
		else:
			print('none', end = ' ')

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = [int(x) for x in line]
			line.sort()
			result(line)