from math import sin, cos, radians
def alpha(x, y):
	alphax = (x + y / 60) % 12 * 360 / 12
	alphay = y * 6
	
	return (alphax, alphay)

def result(hours, minutes):
	alphax, alphay = alpha(hours, minutes)

	x1 = round((10 + 6 * sin(radians(alphax))), 8)
	y1 = round((10 + 6 * sin(radians(90 - alphax))), 8)

	x2 = round((10 + 9 * sin(radians(alphay))), 8)
	y2 = round((10 + 9 * sin(radians(90 - alphay))), 8)

	print(x1, y1, x2, y2, end = ' ')

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			for x in line:
				x = x.split(":")
				x = [int(x) for x in x]
				result(x[0], x[1])