def result(n, x0, a = 445, c = 700001, m = 2097152):
	cnt, vwl = 'bcdfghjklmnprstvwxz', 'aeiou'
	for x in n:
		f, spam = 0, ''
		while len(spam) < x:
			x0 = (a * x0 + c) % m
			if f == 0:
				spam += cnt[x0 % 19]
				f = 1
			else:
				spam += vwl[x0 % 5]
				f = 0
		print(spam, end = ' ')


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		line = list(map(lambda x: int(x), line))
		if len(line) == 2:
			x0 = line[1]
		else:
			result(line, x0)