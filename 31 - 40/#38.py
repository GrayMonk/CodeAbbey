from math import sqrt as sqrt
flag = 0

def result(a, b, c):
	global flag

	if flag != 0:
		print(';', end = ' ')
	
	flag += 1

	if (b**2 - 4*a*c) < 0:
		k = int(-b / (2 * a))
		m = int((sqrt(abs(b**2 - 4*a*c))) / (2 * a))

		x1 = str(int(k)) + '+' + str(int(m)) + 'i'
		x2 = str(int(k)) + '-' + str(int(m)) + 'i'
		print(x1 + ' ' + x2, end = '')
	else:
		if sqrt(b**2 - 4*a*c) > 0:
			x1 = int((-b + sqrt(b**2 - 4*a*c)) / (2 * a))
			x2 = int((-b - sqrt(b**2 - 4*a*c)) / (2 * a))
			if x1 > x2:
				print(str(x1) + ' ' + str(x2), end = '')
			else:
				print(str(x2) + ' ' + str(x1), end = '')
		elif sqrt(b**2 - 4*a*c) == 0:
			x1 = int((-b + sqrt(b**2 - 4*a*c)) / (2 * a))
			print(str(x1) + ' ' + str(x1), end = '')

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = list(map(lambda x: int(x), line))
			result(line[0], line[1], line[2])