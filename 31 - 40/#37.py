def result(p, r, l):
	i = r / 100 / 12
	res = p * (i + i / (((i + 1) ** l) - 1))
	print(round(res))


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		line = list(map(lambda x: int(x), line))
		result(line[0], line[1], line[2])