file = open('input.txt', 'r')

def funk(n, k):
	d, i = [], 1

	while len(d) < n:
		d.append(i)
		i += 1
	
	i = 0
	while len(d) != 1:
		for x in d:
			if i == k - 1:
				if x == d[-1]:
					i = -1
				else:
					i = 0
				d.remove(x)
			
			i += 1
	print(d[0])

for line in file:
	space = line.split(' ')
	funk(int(space[0]), int(space[1]))