from math import sqrt, exp

def result(a, b, c, d):
	lb, rb = 0, 100
	
	for step in range(0, 100):
		xm = (lb + rb) / 2
		xc = a * xm + b * sqrt(xm ** 3) - c * exp(-xm / 50) - d

		if 0 < xc:
			rb = xm
		elif 0 > xc:
			lb = xm
	
	print(("%.12f" % xm), end = ' ')

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = list(map(lambda x: float(x), line))
			result(line[0], line[1], line[2], line[3])