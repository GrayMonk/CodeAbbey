from math import sqrt

def result(data):
	m, md, d = 0, 0, []
	stock = data[0]
	data.remove(stock)
	data = list(map(lambda x: int(x), data))
	
	m = sum(data) / len(data)
	for x in data:
		d.append((m - x) ** 2)
	md = sqrt((sum(d) / len(d)))
	
	if (m / 100) * 4 <= md:
		print(stock, end = ' ')


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			result(line)