file = open('input.txt', 'r')

i, spam = 0, ''

def funk(s, r, p):
	step = 0
	while s < r:
			s = round((s + (s * p / 100)))
			step += 1
	return str(step)

for line in file:
	space = line.split(' ')
	if i != 0:
    		spam = spam + funk(int(space[0]), int(space[1]), int(space[2])) + ' '
	i += 1
print(spam)