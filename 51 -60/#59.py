def tolst(st):
	d = []
	for x in st:
		d.append(x)
	return d

def func(a, o):
	b, c, i = 0, 0, 0
	for x in a:
		if a[i] == o[i]:
			b += 1
		else:
			if x in o:
				c += 1
		i += 1

	return (str(b) + '-' + str(c))

with open('input.txt', 'r') as file:
	spam, opts = '', []
	for line in file:
		space = (line.strip()).split(' ')
		if len(space) == 2:
			opts = tolst(space[0])
		else:
			for x in space:
				spam += func(tolst(x), opts) + ' '
print(spam)