suits = ['Clubs', 'Spades', 'Diamonds', 'Hearts']
ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'Jack', 'Queen', 'King', 'Ace']

def func(a, b, s = ''):
	for x in a:
		s += (ranks[(x % 13)] + '-of-' + suits[(int(x / 13))] + ' ')
	print(s)

with open('input.txt', 'r') as file:
	for line in file:
		space = (line.strip()).split(' ')
		if len(space) != 1:
			func(list(map(lambda x: int(x), space)), len(space))