from math import sqrt
file = open('input.txt', 'r')

i, spam = 0, ''

def funk(a, b, c):

	if a**2+b**2 == c**2:
		return 'R'
	elif max(a, b, c) < sqrt(a**2+b**2):
		return 'A'
	else:
		return 'O'

for line in file:
	space = line.split(' ')
	if i != 0:
		spam = spam + funk(int(space[0]), int(space[1]), int(space[2])) + ' '
	i += 1

print(spam)