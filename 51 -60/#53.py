
def result(data):
	if (data[0][0] == data[1][0]) or (data[0][1] == data[1][1]):
		print('Y', end = ' ')
	else:
		if abs(int(ord(data[0][0])) - int(ord(data[1][0]))) == \
			abs(int(ord(data[0][1])) - int(ord(data[1][1]))):
				print('Y', end = ' ')
		else:
			print('N', end = ' ')


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			result(line)