file = open('input.txt', 'r')

spam = ''

def func(a):
    d, resl = [], ''
    for x in a:
        if a.count(x) != 1 and not (x in d):
            d.append(x)

    for x in sorted(d):
        resl += x + ' '
    return resl

for line in file:
    space = (line.strip()).split(' ')
    spam = spam + func(space) + ' '
print(spam)