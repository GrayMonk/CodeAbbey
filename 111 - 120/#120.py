def getindex(values, item, i = 0):
	while i < len(values):
		if values[i] == item:
			return i
		i += 1
	
def result(values, sorted_list = [], spam = ''):
	while len(values) != 1:
		sorted_list.insert(0, max(values))
		index = getindex(values, max(values))
		spam += str(index) + ' '
		values[index] = values[-1]
		values.pop()

	print(spam)

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = list(map(lambda x: int(x), line))
			result(line)