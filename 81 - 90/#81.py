file = open('input.txt', 'r')

i, spam = 0, ''

def func(a):
	s, i = 0, 0

	if a > 0:
		a = ("{0:032b}".format(a))
	else:
		a = abs(a)
		a = ("{0:032b}".format(a))
		a = a.replace('0', '2')
		a = a.replace('1', '0')
		a = a.replace('2', '1')
		if a[-1::] == '0':
			a = a[0:-1] + '1'
		else:
			a = a[::-1]
			for x in a:
				if x == '1':
					a = a[:i:] + '0' + a[i+1::]
				else:
					a = a[:i:] + '1' + a[i+1::]
					break
				i += 1
			a = a[::-1]

	for x in a:
		if x == '1':
			s += 1
	return str(s)

for line in file:
	space = line.split(' ')
	if i != 0:
			for x in space:		
				spam = spam + func(int(x)) + ' '
	i += 1
print(spam)