def switch(values, x1, x2):
	t = values[x2]
	values[x2] = values[x1]
	values[x1] = t
	return values

def result(line, i = 0, step = 0):
	while i < len(line) - 1:
		step = 0
		if line[i] > line[i + 1]: 
			s = i
			while s >= 0:
				if line[s] > line[s + 1]:
					line = switch(line, s, s + 1)
					step += 1
				s -= 1
		print(step, end = ' ')
		i += 1

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		if len(line) != 1:
			line = [int(x) for x in line]
			result(line)