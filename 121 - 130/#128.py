from math import factorial as ftrl

def result(n, k):
	return str(int(ftrl(n) / (ftrl(k) * ftrl(n - k))))

with open('input.txt', 'r') as file:
	spam = ''
	for line in file:
		space = (line.strip()).split(' ')
		if len(space) != 1:
			spam += result(int(space[0]), int(space[1])) + ' '
print(spam)