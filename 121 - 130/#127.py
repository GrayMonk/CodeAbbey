def result(data, num = 0):
	with open('words.txt', 'r') as words:
		for word in words:
			word = word.strip()
			d = []
			for item in word:
				d.append(item)
			d.sort()
			if d == data:
				num += 1
	print(num - 1, end = ' ')

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		for x in line:
			d = []
			if len(x) != 1:
				for item in x:
					d.append(item)
					d.sort()
				result(d)