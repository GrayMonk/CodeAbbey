file = open('input.txt', 'r')

i, spam = 0, ''


def funk(a):
	lst = []
	step = 0
	while True:
		if lst.count(a):
			break
		
		step += 1
		lst.append(a)
		a = str(int(a) ** 2)
		#a = str(a)
		while len(a) < 8:
			a = '0' + a
		a = a[2:6]
	return str(step)

for line in file:
	space = line.split(' ')
	if i != 0:
		for x in space:
			spam = spam + funk(x) + ' '
	i += 1

print(spam)