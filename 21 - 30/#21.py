file = open('input.txt', 'r')

i, a, k = 0, 0, 1
s, spam = [], ''
mx = 0
res = 0

for line in file:
	space = line.split(' ')

	if i == 0:
		mx = int(space[1])
	else:
		while a < len(space):
			s.insert(a, space[a].strip())
			a += 1
	i += 1

i = 0

while k <= mx:
	while i < len(s):
		if int(s[i]) == k:
			res += 1
		i += 1

	spam = spam + str(res) + ' '
	res = 0
	k += 1
	i = 0
print(spam)