import math
file = open('input.txt', 'r')

spam, i = '', 0

def funk(weight, height):
	if float(weight)/float(height)**2 < 18.5:
		return 'under'
	elif 18.5 <= float(weight)/float(height)**2 < 25.0:
		return 'normal'
	elif 25.0 <= float(weight)/float(height)**2 < 30.0:
		return 'over'
	else:
		return 'obese'

for line in file:
	x = []
	space = line.split(' ')
	if i != 0:
		spam += funk(space[0], space[1]) + ' '
	i += 1

print(spam)