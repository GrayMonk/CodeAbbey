file = open('input.txt', 'r')

arr = []

def funk(arr):
    step, i, c = 0, 0, 0
    while i < (len(arr)-1):
        if arr[i]>arr[i+1]:
            s = arr[i]
            arr[i] = arr[i+1]
            arr[i+1] = s
            step += 1
        i += 1

    for x in arr:
        c += x
        c *= 113
        c %= 10000007
    return (str(step) + ' ' + str(c))

for line in file:
    space = line.split(' ')
    for x in space:
        if int(x) != -1:
            arr.append(int(x))
    print(funk(arr))