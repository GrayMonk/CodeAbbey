file = open('input.txt', 'r')

arr = []
a = 0

def funk(arr):
    step, all, c, ctrl = 0, 0, 0, 1
    while ctrl != 0:
        ctrl = 0
        i = 0
        all += 1        
        while i < (len(arr)-1):
            if arr[i]>arr[i+1]:
                s = arr[i]
                arr[i] = arr[i+1]
                arr[i+1] = s
                step += 1
                ctrl = 1
            i += 1
    return(str(all) + ' ' + str(step))

for line in file:
    space = line.split(' ')
    if a != 0:
        for x in space:
            arr.append(int(x))
        print(funk(arr))
    a += 1