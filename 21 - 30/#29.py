file = open('input.txt', 'r')

a = 0
dict = {}

def funk(dict):
    lst, ans = [], ''

    for x in dict:
        lst.append(dict.get(x))

    lst.sort()
    inv_d = {v:k for k, v in dict.items()}
    for x in lst:
        ans += str(inv_d.get(x)) + ' '

    return ans

for line in file:
    space = line.split(' ')
    if a != 0:
        s = 1
        for x in space:
            dict[s] = int(x)
            s += 1
    a += 1
print(funk(dict))