file = open('input.txt', 'r')

i, spam = 0, ''

def funk(a, c, m, x, n):
	step = 0
	while step < n:
		x = (a * x + c) % m
		step += 1
	return str(x)

for line in file:
	space = line.split(' ')
	if i != 0:
    		spam = spam + funk(int(space[0]), int(space[1]),\
				int(space[2]), int(space[3]), int(space[4])) + ' '
	i += 1
print(spam)