file = open('input.txt', 'r')

a = 0
spam = ''

def funk(x, y):
    lcm, x1, y1 = 0, 0, 0
    x1 = x
    y1 = y
    while x1 != y1:
        if x1 < y1:
            y1 -= x1
        else:
            x1 -= y1
    
    lcm = x * y / x1

    return (str('(' + str(int(x1)) + ' ' + str(int(lcm)) + ')'))

for line in file:
    space = line.split(' ')
    if a != 0:
        spam = spam + funk(int(space[0]), int(space[1])) + ' '
    a += 1
print(spam)