def result(case):
	x, y, n = int(case[0]), int(case[1]), int(case[2])
	nx = int(n * y / (x + y))
	print(min((n - nx) * y, (nx + 1) * x), end = ' ')

with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split()
		if len(line) != 1:
			result(line)