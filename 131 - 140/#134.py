def result(line):
	x, y = 0, 0
	flagx, flagy = True, True
	for z in range(0, 101):
		
		print(x, end = ' ')
		if flagx:
			if x <= (line[0] - line[2]): x += 1
			if x == (line[0] - line[2]): flagx = False
		else:
			if (x - 1) >= 0: x -= 1
			if x == 0: flagx = True

		print(y, end = ' ')
		if flagy:
			if y < line[1]: y += 1
			if y == line[1] - 1: flagy = False
		else:
			if y - 1 >= 0: y -= 1
			if y == 0: flagy = True


with open('input.txt', 'r') as file:
	for line in file:
		line = (line.strip()).split(' ')
		line = [int(x) for x in line]
		result(line)