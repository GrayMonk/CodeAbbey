from math import sqrt, pow
file = open('input.txt', 'r')

i, spam = 0, ''
def func(x1, y1, x2, y2, x3, y3):
	a, b, c, p, s = 0, 0, 0, 0, 0
	
	a = round(sqrt(pow((x1 - x2), 2) + pow((y1 - y2), 2)), 7)
	b = round(sqrt(pow((x1 - x3), 2) + pow((y1 - y3), 2)), 7)
	c = round(sqrt(pow((x2 - x3), 2) + pow((y2 - y3), 2)), 7)

	p = round((a + b + c) * 0.5, 7)
	s = round(sqrt(p * (p - a) * (p - b) * (p - c)), 7)

	return str(s)

for line in file:
	space = (line.strip()).split(' ')
	if i != 0:
		spam = spam + str(func(int(space[0]), int(space[1]), \
			int(space[2]), int(space[3]), int(space[4]), int(space[5]))) + ' '
	i += 1
print(spam)